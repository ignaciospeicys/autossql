package com.captton.autos.controller;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.captton.autos.model.Auto;
import com.captton.autos.repository.AutoDao;



@RestController
@RequestMapping({"/autos"})
public class MainController {
	
	@Autowired
	AutoDao autodao;
	
	//doy de alta un auto
	@PostMapping
	public Auto create(@RequestBody Auto auto) {
		
		return autodao.save(auto);
	}
	
	//doy de baja un auto
	@DeleteMapping(path = {"/{id}"})
	public ResponseEntity<Object> delete(@PathVariable("id") long id) {
		
		autodao.deleteById(id);
		
		return ResponseEntity.ok().body("Auto borrado del registro");
	}
	
	//actualizo datos en un id dado (update)
	@PutMapping(value ="/{id}")
	public ResponseEntity<Auto> update(@PathVariable("id") long id, @RequestBody Auto auto){
		
		return autodao.findById(id).map(record -> {
			record.setModelo(auto.getModelo());
			record.setMarca(auto.getMarca());
			record.setPatente(auto.getPatente());
			record.setColor(auto.getColor());
			
			Auto updated = autodao.save(record);
			
			return ResponseEntity.ok().body(updated);
			
		}).orElse(ResponseEntity.notFound().build());
	}
	
	//listo todos los autos
	@GetMapping
	public ResponseEntity<Object> findAll() {
		
		List<Auto> autosFound = autodao.findAll();
		
		if(autosFound.size() > 0) {
			
			for(Auto au: autosFound) {
				
				System.out.println("Patente: "+au.getPatente());
			}
			
			JSONObject obj = new JSONObject();
			
			obj.put("error", 0);
			obj.put("results", autosFound);
			return ResponseEntity.ok().body(obj.toString());
			
		} else {
			
			JSONObject obj = new JSONObject();
			obj.put("error", 1);
			obj.put("results", "no se encontraron autos");
			return ResponseEntity.ok().body(obj.toString());
			
		}
		
	}
	
	//busco por patente en lugar de Id
	@GetMapping(path= {"/buscar/{pat}"})
	public ResponseEntity<Object> findByPatente(@PathVariable("pat") String patente) {
		
		List<Auto> autosFound = autodao.findByPatente(patente);
		
		if(autosFound.size() > 0) {
			
			for(Auto au : autosFound) {
				
				System.out.println("Patente: "+au.getPatente());
			}
			
			JSONObject obj = new JSONObject();
			
			obj.put("error", 0);
			obj.put("results", autosFound);
			
			return ResponseEntity.ok().body(obj.toString());
	
		} else {
			
			JSONObject obj = new JSONObject();
			
			obj.put("error", 1);
			obj.put("results", "no se encontraron autos en la lista");
			
			return ResponseEntity.ok().body(obj.toString());
			
		}
	}

}
