package com.captton.autos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.autos.model.Auto;

public interface AutoDao extends JpaRepository<Auto, Long> {
	
	public List<Auto> findByPatente(String patente);

}
